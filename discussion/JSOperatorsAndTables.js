console.log ("Hello World")

// Assignment Operators 

	// Basic Assignment Operator (=)
        // The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

		let assignmentNumber = 8;
		console.log (assignmentNumber)

	// Addition Assignment Operator 


		let totalNumber = assignmentNumber + 2;
		console.log ("Result of addition assignment operator :" + totalNumber );

	// Arithmetic Operator (+, -, *, /, %)

		let x = 235;
		let y = 2358;

		let sum = x + y; 
		console.log ("Result of addition operator: " + sum);

		let difference = x - y;
		console.log ("Result of subtraction operator: " + difference);

		let product = x * y;
		console.log ("Result of multiplication operator: " + product);

		let quotient = y / x;
		console.log ("Result of quotient operator: " + quotient);

		let remainder = y % x; 
		console.log ("Result of modulo operator: " + remainder);

	// Mini Activity: did mini activity

	// Multiple Operators and Parentheses (MDAS and PEMDAS)

	/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
    
    The operations were done in the following order:
        1. 3 * 4 = 12
        2. 12 / 5 = 2.4
        3. 1 + 2 = 3
        4. 3 - 2.4 = 0.6 
    */

	let mDas1 = 1 + 2 - 3 * 4 / 5; 
	let mDas2 = 6 / 2 * 1 + 2
	console.log ("Result of mDas1 operation: " + mDas1);
	console.log ("Result of mDas2 operation: " + mDas2);

	let pemdas = 1 + (2 - 3) * (4 / 5); 

		/*
		1. 4 / 5 = 0.8 
		2. 2 - 3 = -1
		3. -1 + 0.8 = -0.8
		4. 1 + (-0.8) = .2
		*/
	console.log ('Result of PEMDAS ' + pemdas)

	// Comparison Operator 
		// (paghahambing, true or false)

	// Equality operator (==)

		/*
			- Checks whether the operands are equal / have the same content. 
			- Note: = is for assignment operator
			- Note: == is for equality operator
			- Attemps to convert and compare operands of different data types. 
			- True == 1 and False == 0
		*/

		let juan = 'juan'

		console.log(1 == 1); // True
		console.log(1 == 2); // False
		console.log(1 == '1'); // True
		console.log(0 == false); //False
		//Compares two strings that are the same 
		console.log('juan' == 'juan');
		//Compares a string with the variable "juan" declared above 
		console.log('juan' == juan); // True 

		console.log 


	// Inequality Operator (!=)

		/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
        */

        console.log(1 != 1); //False
        console.log(1 != 2); //True
        console.log(1 != '1'); // False
        console.log(0 != false); // False
        console.log('juan' != 'juan'); // False
        console.log('juan' != juan); // False

  	// Strict Equality Operator (===)

        /* 
			- Checks whether the operands are equal/have the same content
			 - Also COMPARES the data types of 2 values
			 - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
			- In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
			 - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
			- Strict equality operators are better to use in most cases to ensure that data types provided are correct
		*/

		console.log(1 === 1); // True
		console.log(1 === 2); // False
		console.log(1 === '1'); // False
		console.log(0 === false); // False
		console.log('juan' === 'juan'); // True 
		console.log('juan' === juan); // True

	// Relational Operators 

		//Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65 // False
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65 // True
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65 false
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65 // True 

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual)

    // Logical Operators (&&, ||, !)

        let isLegalAge = true; 
        let isRegistered = false; 

        // Logical Operator (&& - Double Ampersand - and Operator)

        // Return true if all operands are true 
        // Note true = 1 false = 0

        // 1 && 1 = True
        // 1 && 0 = False
        // 0 && 1 = False
        // 0 && 0 = False

    let allRequirementsMet = isLegalAge && isRegistered
    console.log ("Result of Logical AND operator " + allRequirementsMet);


	    // Logical or Operator (|| - Double Pipe)
	    // Returns true if one of the operands are true. 

	    // 1 || 1 = True
	    // 1 || 0 = True
	    // 0 || 1 = True
	    // 0 || 0 = True

    let someRequirementsMet = isLegalAge || isRegistered; 

    console.log ("Results of logical OR operator " + someRequirementsMet)


    // Logical Not Operator (! - Exclamation Point)
        // Returns the opposite value 
    let someRequirementsNotMet = !isRegistered;
    console.log("Result of logical NOT Operator: " + someRequirementsNotMet); // true

    // legal - true isRegistred - false
    
    let Requirements = !isLegalAge || !isRegistered
    console.log (Requirements)


























